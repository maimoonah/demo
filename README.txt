DOCKER DEMO with python

Getting started with Docker:
    - The fastest and easiest way to get started with Docker is to download Docker for Desktop.
    - Download Docker using the link below and select your preferred operating system.

A simple 'Hello, world!' python application called app.py has been included. To build and run this application as a docker image,
we will first create a Dockerfile. 

IMAGES:

    Building an image:
        - A Dockerfile is used to build an image. Create a file named 'Dockerfile' in the project folder and write out the following code
            # syntax=docker/dockerfile:1

            FROM python:3.8-slim-buster

            WORKDIR /app

            COPY requirements.txt requirements.txt
            RUN pip3 install -r requirements.txt

            COPY . .

            CMD [ "python3", "-m", "flask", "run", "--host=0.0.0.0"]

    
        - Open a terminal and go to the directory with the Dockerfile and use the following docker build command to build an image
            docker build -t python-docker .
                -t is a flag to name the image and optionally give it a tag. Alternatively, --tag can be used in place of -t
                python-docker is the name of the image
                . is used to build the image in the current working directory

    Tag an image:
        - To tag an image, in a terminal use the following command
            docker tag 'image name' 'image name':v1
            A tag is optional. If a tag is not passed, "latest" is used by Docker as the default tag

            E.g.   docker tag python-docker:latest python-docker:v1.0.0
        
        - To remove a tag, use the rmi command
            docker rmi python-docker:v1.0.0

    Check the images built:
        - Use the following command
            docker images

    Delete/Remove an image:
        - Use the following command
            docker images rm 'image name'
            E.g.   docker images rm python-docker
    
    Run an image:
        - Use the following command to run the application:
            docker run -d -p 8080:80 'image name'
                -d runs the container in detached mode
                -p 8080:80 maps the host port to the container port
                'image name'is the image to be used







